// Прелоадер
$( document ).ready( play );

function play() {
  var tl = new TimelineMax({onComplete:finish});
  tl.staggerTo(".oz", 1.5, {css:{opacity: 1, transform: "scale(1)"}, ease:Power4.easeOut, force3D:true, yoyo:true, repeat:1}, 0.1);
};

function finish() {
  TweenLite.to("#preloader", 0.66, {opacity:0, autoAlpha:0});
  $("body").css({"position":"absolute", "height":"auto"});
};

// Затемнение при переходе по ссылке
$(function() {
  $('a').click(function(){
    var href= $(this).attr('href');

    TweenLite.to("body", 0.66, {opacity:0, autoAlpha:1, onComplete:redirect});

    function redirect() { window.location=href; }
    
    return false;
  });
});