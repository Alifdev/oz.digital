// ROTATE ICON
$(document).ready(function(){
	var tl = new TimelineMax({onComplete:play});
	tl.to(".icon-gear", 3, {rotation:360, ease:Power0.easeNone});

	function play() {
		tl.play(0);
	}
});

// SLIDER
$(document).ready(function(){
	$('.cases-wrapper').slick({
		easing: "easeOutCubic",
		dots: true,
		arrows: false,
		draggable: true,
		swipe: true,
		touchMove: true,
		infinite: false,
		speed: 500,
		slidesToShow: 1,
		autoplay: false,
		autoplaySpeed: 2000
	});

  // Видео в слайдере
});

$(document).ready(function(){
var $video = $('.video-bg');
  $video.on('canplaythrough', function() {
     this.play();
  });
});

// Форма обратной связи
$(document).ready(function(){

	var tl = new TimelineMax({paused:true});

	tl.staggerFrom(".form", 0.66, {css:{opacity: "0"}, }, 0.05, 0)
  .fromTo("#popup_contact", 0.66, {autoAlpha:0, scale:1.5, css:{top:"100%"}, ease: Expo.easeOut}, {utoAlpha:1, scale:1, css:{top:"0",  opacity:"1"}}, 0 )
  .fromTo("#main-content", 0.66, {opacity:1, y:0, ease: Expo.easeOut}, {opacity:0, y:-100}, 0 )

	$(".open-form-btn").click(function() {
		tl.play();
	});

	$(".popup_cross").click(function() {
		tl.reverse();
	});

});

// Форма обратной связии: Загрузить файл
$('#upload_file').change(function(){
  
  var filename = $('#upload_file').val().replace(/C:\\fakepath\\/i, '');
  
  $('#name_file').html(filename);
  
  if ($('#upload_file').get(0).files.length === 0) { $('#name_file').html("Файл не выбран"); }
  
});

// Липкий хидер
$(function() {
  $(window).scroll(function() {
    if ($(this).scrollTop() > 74) {  
      $('.page-header').addClass("sticky");
    }
    else{
      $('.page-header').removeClass("sticky");
    }
  });
});

// Боковое меню на мобильных
$(function() {
  $('#icon-menu').on('click', function(event) {
    $('.page-header').addClass("active");
  });
  $('#icon-menu-cross').on('click', function(event) {
    $('.page-header').removeClass("active");
  });
});