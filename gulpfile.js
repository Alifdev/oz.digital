var gulp = require('gulp'),
	haml = require('gulp-ruby-haml'),
  htmlmin = require('gulp-htmlmin'),
  htmlhint = require("gulp-htmlhint"),
	sass = require('gulp-sass'),
  sourcemaps = require('gulp-sourcemaps'),
  autoprefixer = require('gulp-autoprefixer'),
  shorthand = require('gulp-shorthand'),
  uncss = require('gulp-uncss'),
  csso = require('gulp-csso'),
  csscomb = require('gulp-csscomb'),
  imacss = require('gulp-imacss'),
  imagemin = require('gulp-imagemin'),
  browserSync = require('browser-sync');

// Compile sass
gulp.task('sass', function(){ // Создаем таск Sass
    return gulp.src('app/sass/**/*.sass') // Берем источник
        // .pipe(sourcemaps.init())
        .pipe(sass()) // Преобразуем Sass в CSS посредством gulp-sass
        // .pipe(autoprefixer()) // Проставляем префиксы
        // .pipe(uncss({
        //     html: ['index.html', 'app/**/*.html', 'http://oz.digital']
        // })) // Очищаем CSS от неиспользованного кода
        // .pipe(shorthand()) // Сокращаем CSS короткими записями
        // .pipe(csso({
        //     restructure: falsse,
        //     sourceMap: true,
        //     debug: true
        // })) // Минимизируем CSS
        // .pipe(csscomb()) // Расставляем свойства по дзену
        // .pipe(sourcemaps.write())
        .pipe(gulp.dest('app/css')) // Выгружаем результата в папку app/css
        .pipe(browserSync.reload({stream: true})) // Обновляем CSS на странице при изменении
});

// Complie haml
gulp.task('haml', function () {
  gulp.src(['app/haml/**/*.haml', '!**/_*/**'])
    .pipe(haml())
    // .pipe(htmlmin({collapseWhitespace: true}))
    // .pipe(htmlhint())
    // .pipe(htmlhint.reporter())
    .pipe(gulp.dest('app/'));
});

// Compress images
// gulp.task('imagemin', function () {
//   gulp.src('app/images/**/*')
//     .pipe(imagemin())
//     .pipe(gulp.dest('app/images-compress'))
// });

// Live reload
gulp.task('browser-sync', function() { // Создаем таск browser-sync
    browserSync({ // Выполняем browser Sync
        server: { // Определяем параметры сервера
            baseDir: 'app' // Директория для сервера - app
        },
        notify: false // Отключаем уведомления
    });
});


// Watch
gulp.task('watch', ['browser-sync', 'sass', 'haml'], function() {
    gulp.watch('app/sass/**/*.sass', ['sass']);
    gulp.watch('app/haml/**/*.haml', ['haml']);
    gulp.watch('app/*.html', browserSync.reload);
    gulp.watch('app/js/**/*.js', browserSync.reload); // Наблюдение за JS файлами в папке js
});

gulp.task('clear', function () {
    return cache.clearAll();
})

gulp.task('default', ['watch']);


